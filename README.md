# Teensy_SX1262_LoRaWAN

Arduino port of the Semtech LoRaMAC-node library for the SX1262 + Teensy 4.1. Based on LoRaMAC-node @ commit 643b12c4f1c6c050457c26da3e02c95e837e6727 (Jun 10, 2020)