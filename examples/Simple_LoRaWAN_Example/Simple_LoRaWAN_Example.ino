#include <SPI.h>
#include <Teensy_SX1262_LoRaWAN.h>

Teensy_SX1262_LoRaWAN lorawan;

void setup() 
{
  lorawan.init();
}

void loop() 
{
  lorawan.task();
}