#include "Teensy_SX1262_LoRaWAN.h"
#include "LoRaMac.h"
#include "board.h"

#define LORAWAN_DEFAULT_DATARATE DR_0
#define ACTIVE_REGION LORAMAC_REGION_US915
#define LORAWAN_PUBLIC_NETWORK                             true
#define LORAWAN_ADR_ON                              1
#define LORAWAN_APP_PORT 2

/***************************************************************************
  Semtech
***************************************************************************/

static void JoinNetwork( void )
{
  LoRaMacStatus_t status;
  MlmeReq_t mlmeReq;
  mlmeReq.Type = MLME_JOIN;
  mlmeReq.Req.Join.Datarate = LORAWAN_DEFAULT_DATARATE;

  // Starts the join procedure
  status = LoRaMacMlmeRequest( &mlmeReq );
}

static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm )
{
  switch ( mlmeConfirm->MlmeRequest )
  {
    case MLME_JOIN:
      {
        if ( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK )
        {
          // Joined successfully
        }
        else
        {
          // Join was not successful. Try to join again
          JoinNetwork( );
        }
        break;
      }
    default:
      break;
  }
}

static void McpsIndication( McpsIndication_t *mcpsIndication )
{

}

static void McpsConfirm( McpsConfirm_t *mcpsConfirm )
{

}

static void MlmeIndication( MlmeIndication_t *mlmeIndication )
{

}

static void PrepareTxFrame( uint8_t port )
{
    
}

static bool SendFrame( void )
{
   return 1;
}

/***************************************************************************
  Teensy Library
***************************************************************************/

Teensy_SX1262_LoRaWAN::Teensy_SX1262_LoRaWAN()
{

}

bool Teensy_SX1262_LoRaWAN::init(void)
{
  LoRaMacPrimitives_t macPrimitives;
  LoRaMacCallback_t macCallbacks;
  MibRequestConfirm_t mibReq;
  LoRaMacStatus_t status;
  uint8_t devEui[8] = { 0 };  // Automatically filed from secure-element
  uint8_t joinEui[8] = { 0 }; // Automatically filed from secure-element
  uint8_t sePin[4] = { 0 };   // Automatically filed from secure-element
  
  BoardInitMcu( );

  macPrimitives.MacMcpsConfirm = McpsConfirm;
  macPrimitives.MacMcpsIndication = McpsIndication;
  macPrimitives.MacMlmeConfirm = MlmeConfirm;
  macPrimitives.MacMlmeIndication = MlmeIndication;
  //macCallbacks.GetBatteryLevel = BoardGetBatteryLevel;
  //macCallbacks.GetTemperatureLevel = NULL;
  //macCallbacks.NvmContextChange = NvmCtxMgmtEvent;
  //macCallbacks.MacProcessNotify = OnMacProcessNotify;

  status = LoRaMacInitialization( &macPrimitives, &macCallbacks, ACTIVE_REGION );
  if ( status != LORAMAC_STATUS_OK )
  {
    // Fatal error, endless loop.
    while ( 1 )
    {
    }
  }

  mibReq.Type = MIB_DEV_EUI;
  LoRaMacMibGetRequestConfirm( &mibReq );
  memcpy1( devEui, mibReq.Param.DevEui, 8 );

  mibReq.Type = MIB_JOIN_EUI;
  LoRaMacMibGetRequestConfirm( &mibReq );
  memcpy1( joinEui, mibReq.Param.JoinEui, 8 );

  mibReq.Type = MIB_SE_PIN;
  LoRaMacMibGetRequestConfirm( &mibReq );
  memcpy1( sePin, mibReq.Param.SePin, 4 );

  mibReq.Type = MIB_PUBLIC_NETWORK;
  mibReq.Param.EnablePublicNetwork = LORAWAN_PUBLIC_NETWORK;
  LoRaMacMibSetRequestConfirm( &mibReq );

  mibReq.Type = MIB_ADR;
  mibReq.Param.AdrEnable = LORAWAN_ADR_ON;
  LoRaMacMibSetRequestConfirm( &mibReq );

  mibReq.Type = MIB_SYSTEM_MAX_RX_ERROR;
  mibReq.Param.SystemMaxRxError = 20;
  LoRaMacMibSetRequestConfirm( &mibReq );

  LoRaMacStart( );

  mibReq.Type = MIB_NETWORK_ACTIVATION;
  status = LoRaMacMibGetRequestConfirm( &mibReq );

  if ( status == LORAMAC_STATUS_OK )
  {
    if ( mibReq.Param.NetworkActivation == ACTIVATION_TYPE_NONE )
    {
      mibReq.Type = MIB_DEV_EUI;
      LoRaMacMibGetRequestConfirm( &mibReq );

      mibReq.Type = MIB_JOIN_EUI;
      LoRaMacMibGetRequestConfirm( &mibReq );

      mibReq.Type = MIB_SE_PIN;
      LoRaMacMibGetRequestConfirm( &mibReq );

      JoinNetwork( );
    }
  }

  return 1;
}

bool Teensy_SX1262_LoRaWAN::task(void)
{
  // Process Radio IRQ
  if ( Radio.IrqProcess != NULL )
  {
    Radio.IrqProcess( );
  }
  // Processes the LoRaMac events
  LoRaMacProcess( );

  return true;
}

void Teensy_SX1262_LoRaWAN::sendPacket(void)
{
  PrepareTxFrame( LORAWAN_APP_PORT );
  SendFrame( );
}