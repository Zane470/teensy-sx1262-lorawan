#ifndef TEENSY_SX1262_LORAWAN_H
#define TEENSY_SX1262_LORAWAN_H

class Teensy_SX1262_LoRaWAN
{
	public:

		Teensy_SX1262_LoRaWAN();
		bool init(void);
		bool task(void);
    void sendPacket(void);

	private:
	
};

#endif