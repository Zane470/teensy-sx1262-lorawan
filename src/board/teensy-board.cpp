#include <Arduino.h>
#include <SPI.h>
#include "sx126x-board.h"
#include <TimerThree.h>
#include <TeensyID.h>
#include "../timer.h"

/***************************************************************************
  Defines
***************************************************************************/

#define SX1262_BUSY_PIN 41
#define SX1262_CS_PIN 40
#define SX1262_DIO1_PIN 39
#define SX1262_DIO3_PIN 38
#define SX1262_RESET_PIN 37

#define BOARD_TCXO_WAKEUP_TIME 5

extern "C" {
  
  /***************************************************************************
    Init
  ***************************************************************************/
  
  void BoardInitMcu( void )
  {
    // Init timer3, make sure it's stopped
    Timer3.initialize(1000 * 1000);
    Timer3.disablePwm(6);
    Timer3.disablePwm(9);
    Timer3.stop();
    
    // Init GPIO
    pinMode(SX1262_BUSY_PIN, INPUT);
    pinMode(SX1262_CS_PIN, OUTPUT);
    pinMode(SX1262_DIO1_PIN, INPUT);
    pinMode(SX1262_DIO3_PIN, INPUT);
    pinMode(SX1262_RESET_PIN, OUTPUT);
    digitalWrite(SX1262_CS_PIN, HIGH);
    digitalWrite(SX1262_RESET_PIN, HIGH);
    
    // Init spi
    SPI.begin(); 
  }

  /***************************************************************************
    Delay
  ***************************************************************************/

  void DelayMsMcu( uint32_t ms )
  {
    delay(ms);
  }

  /***************************************************************************
    SPI
  ***************************************************************************/

  void SX126xReadRegisters( uint16_t address, uint8_t *buffer, uint16_t size )
  {
    uint8_t addr_l, addr_h;

    addr_h = address >> 8;
    addr_l = address & 0x00FF;

    SX126xCheckDeviceReady( );

    digitalWrite(SX1262_CS_PIN, LOW);
    SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));
    
    SPI.transfer(RADIO_READ_REGISTER);
    SPI.transfer(addr_h);               //MSB
    SPI.transfer(addr_l);               //LSB
    SPI.transfer(0xFF);
    for (uint16_t index = 0; index < size; index++)
    {
      *(buffer + index) = SPI.transfer(0xFF);
    }
    
    SPI.endTransaction();
    digitalWrite(SX1262_CS_PIN, HIGH);

    SX126xWaitOnBusy( );
  }

  uint8_t SX126xReadRegister( uint16_t address )
  {
    uint8_t data;
    SX126xReadRegisters( address, &data, 1 );
    return data;
  }

  void SX126xWriteRegisters( uint16_t address, uint8_t *buffer, uint16_t size )
  {
    uint8_t addr_l, addr_h;

    addr_l = address & 0xff;
    addr_h = address >> 8;

    SX126xCheckDeviceReady( );

    digitalWrite(SX1262_CS_PIN, LOW);
    SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));

    SPI.transfer(RADIO_WRITE_REGISTER);
    SPI.transfer(addr_h);   //MSB
    SPI.transfer(addr_l);   //LSB

    for (uint16_t i = 0; i < size; i++)
    {
      SPI.transfer(buffer[i]);
    }
    
    SPI.endTransaction();
    digitalWrite(SX1262_CS_PIN, HIGH);

    SX126xWaitOnBusy( );
  }

  void SX126xWriteRegister( uint16_t address, uint8_t value )
  {
    SX126xWriteRegisters( address, &value, 1 );
  }

  void SX126xWriteCommand( RadioCommands_t command, uint8_t *buffer, uint16_t size )
  {
    SX126xCheckDeviceReady( );

    digitalWrite(SX1262_CS_PIN, LOW);
    SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));

    SPI.transfer( ( uint8_t )command );

    for (uint16_t index = 0; index < size; index++)
    {
      SPI.transfer(buffer[index]);
    }
    
    SPI.endTransaction();
    digitalWrite(SX1262_CS_PIN, HIGH);

    if ( command != RADIO_SET_SLEEP )
    {
      SX126xWaitOnBusy( );
    }
  }

  uint8_t SX126xReadCommand( RadioCommands_t command, uint8_t *buffer, uint16_t size )
  {
    uint8_t status = 0;

    SX126xCheckDeviceReady( );

    digitalWrite(SX1262_CS_PIN, LOW);
    SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));

    SPI.transfer( ( uint8_t )command );
    status = SPI.transfer(0x00);

    for ( uint16_t i = 0; i < size; i++ )
    {

      *(buffer + i) = SPI.transfer(0xFF);
    }
    
    SPI.endTransaction();
    digitalWrite(SX1262_CS_PIN, HIGH);

    SX126xWaitOnBusy( );

    return status;
  }

  void SX126xWriteBuffer( uint8_t offset, uint8_t *buffer, uint8_t size )
  {
    SX126xCheckDeviceReady( );

    digitalWrite(SX1262_CS_PIN, LOW);
    SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));

    SPI.transfer(RADIO_WRITE_BUFFER);
    SPI.transfer(offset);

    for (uint16_t i = 0; i < size; i++)
    {
      SPI.transfer(buffer[i]);
    }
    
    SPI.endTransaction();
    digitalWrite(SX1262_CS_PIN, HIGH);

    SX126xWaitOnBusy( );

  }

  void SX126xReadBuffer( uint8_t offset, uint8_t *buffer, uint8_t size )
  {
    SX126xCheckDeviceReady( );

    digitalWrite(SX1262_CS_PIN, LOW);
    SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));

    SPI.transfer(RADIO_READ_BUFFER);
    SPI.transfer(offset);
    SPI.transfer(0);

    for (uint16_t index = 0; index < size; index++)
    {
      *(buffer + index) = SPI.transfer(0xFF);
    }
    
    SPI.endTransaction();
    digitalWrite(SX1262_CS_PIN, HIGH);

    SX126xWaitOnBusy( );
  }

  void SX126xSetRfTxPower( int8_t power )
  {
    SX126xSetTxParams( power, RADIO_RAMP_40_US );
  }

  uint32_t SX126xGetBoardTcxoWakeupTime( void )
  {
    return BOARD_TCXO_WAKEUP_TIME;
  }


  void SX126xIoTcxoInit( void )
  {
    // ???
  }

  void SX126xAntSwOn( void )
  {
    // ???
  }

  void SX126xAntSwOff( void )
  {
    // ???
  }

  void SX126xWaitOnBusy( void )
  {
    while ( digitalRead(SX1262_BUSY_PIN) == 1 );
  }

  uint8_t SX126xGetDeviceId( void )
  {
    return SX1262;
  }

  void SX126xReset( void )
  {
    delay( 10 );
    digitalWrite(SX1262_RESET_PIN, LOW);
    delay( 20 );
    digitalWrite(SX1262_RESET_PIN, HIGH);
    delay( 10 );
  }

  void SX126xIoIrqInit( DioIrqHandler dioIrq )
  {
    attachInterrupt(digitalPinToInterrupt(SX1262_DIO1_PIN), (void (*)())dioIrq, RISING);
  }

  void SX126xWakeup( void )
  {
    //CRITICAL_SECTION_BEGIN( );

    digitalWrite(SX1262_CS_PIN, LOW);
    SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));

    SPI.transfer(RADIO_GET_STATUS);
    SPI.transfer(0x00);
    
    SPI.endTransaction();
    digitalWrite(SX1262_CS_PIN, HIGH);

    SX126xWaitOnBusy( );

    //CRITICAL_SECTION_END( );
  }

  /***************************************************************************
    Interrupt Handling
  ***************************************************************************/

  void BoardCriticalSectionBegin( uint32_t *mask )
  {

  }

  void BoardCriticalSectionEnd( uint32_t *mask )
  {

  }

  /***************************************************************************
    RTC
  ***************************************************************************/

  uint32_t saved_time;

  uint32_t RtcGetCalendarTime( uint16_t *milliseconds )
  {
    *milliseconds = millis();

    return millis() / 1000;
  }

  uint32_t RtcGetMinimumTimeout( void )
  {
    return 1;
  }

  uint32_t RtcGetTimerElapsedTime( void )
  {
    return (millis() - saved_time);
  }

  uint32_t RtcSetTimerContext( void )
  {
    saved_time = millis();

    return saved_time;
  }

  uint32_t RtcGetTimerContext( void )
  {
    return saved_time;
  }

  uint32_t RtcGetTimerValue( void )
  {
    return millis();
  }

  void RtcSetAlarm( uint32_t timeout )
  {
    Timer3.setPeriod(timeout * 1000);
    Timer3.attachInterrupt(TimerIrqHandler);
    Timer3.restart();
  }

  void RtcStopAlarm( void )
  {
    Timer3.stop();
  }

  uint32_t RtcMs2Tick( uint32_t milliseconds )
  {
    return milliseconds;
  }

  uint32_t RtcTick2Ms( uint32_t tick )
  {
    return tick;
  }

  void RtcBkupRead( uint32_t *data0, uint32_t *data1 )
  {
    // NEED TO IMPLEMENT data0 = seconds, data1 = subseconds

    //*data0 = HAL_RTCEx_BKUPRead( &RtcHandle, RTC_BKP_DR0 );
    //*data1 = HAL_RTCEx_BKUPRead( &RtcHandle, RTC_BKP_DR1 );
  }

  void RtcBkupWrite( uint32_t data0, uint32_t data1 )
  {
    // NEED TO IMPLEMENT data0 = seconds, data1 = subseconds

    //HAL_RTCEx_BKUPWrite( &RtcHandle, RTC_BKP_DR0, data0 );
    //HAL_RTCEx_BKUPWrite( &RtcHandle, RTC_BKP_DR1, data1 );
  }

  /***************************************************************************
    Other Stuff
  ***************************************************************************/

  void BoardGetUniqueId( uint8_t *id )
  {
    teensyUID64(id);
  }

}
